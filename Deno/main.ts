import { Neuron } from "./neuron.ts";

function validGateType(gateType: string): boolean {
  return (gateType === "or" || gateType === "and" || gateType === "nand");
}

function generateInputs(numInputs: number): boolean[] {
  const inputs: boolean[] = [];

  for (let i = 0; i < numInputs; i++) {
    inputs.push(Math.random() >= 0.5);
  }

  return inputs;
}

function generateAnswer(inputs: boolean[], type: string): boolean {
  let answer = inputs[0];

  for (let i = 0; i < inputs.length; i++) {
    if (type === "or") {
      answer = answer || inputs[i]; // learn to be an OR gate
    } else {
      answer = answer && inputs[i]; // learn to be an AND gate
    }
  }

  if (type === "nand") {
    answer = !answer; // NAND gate
  }

  return answer;
}

function check(neuron: Neuron, inputs: boolean[], answer: boolean) {
  if (neuron.forwardBinaryCheck(inputs, answer)) {
    console.log(`${inputs}: ${answer} => correct`);
  } else {
    console.log(`${inputs}: ${answer} => incorrect`);
  }
}

function verifyTraining(neuron: Neuron, type: string) {
  switch (type) {
    case "or":
      // OR Gate
      check(neuron, [false, false], false);
      check(neuron, [false, true], true);
      check(neuron, [true, false], true);
      check(neuron, [true, true], true);
      break;

    case "and":
      // AND Gate
      check(neuron, [false, false], false);
      check(neuron, [false, true], false);
      check(neuron, [true, false], false);
      check(neuron, [true, true], true);
      break;

    case "nand":
      // NAND Gate
      check(neuron, [false, false], true);
      check(neuron, [false, true], true);
      check(neuron, [true, false], true);
      check(neuron, [true, true], false);
      break;
  }
}

if (import.meta.main) {
  const gateType = Deno.args[0];
  const numInputs = 2;
  const reps = 1000;

  if (validGateType(gateType)) {
    const neuron = new Neuron(numInputs);

    //const results: boolean[] = [];

    for (let rep = 0; rep < reps; rep++) {
      const inputs = generateInputs(numInputs);
      const answer = generateAnswer(inputs, gateType);

      //results.push(neuron.train(inputs, answer));
      neuron.train(inputs, answer);
    }

    //for (let i = 0; i < results.length; i++) {
    //  console.log("result: " + results[i]);
    //}

    verifyTraining(neuron, gateType);
  } else {
    console.log("input a valid gate type");
  }
}
