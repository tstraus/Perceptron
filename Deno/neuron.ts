export class Neuron {
  threshold = 0.9;
  learningRate = 0.01;
  weights: number[] = [];

  constructor(numInputs: number) {
    for (let i = 0; i < numInputs; i++) {
      this.weights.push(1 / numInputs);
    }
  }

  forward(inputs: boolean[]) {
    if (inputs instanceof Array && inputs.length === this.weights.length) {
      let sum = 0;

      for (let i = 0; i < inputs.length; i++) {
        sum += Number(inputs[i]) * this.weights[i];
      }

      return Math.tanh(sum);
    }

    return -1;
  }

  forwardBinary(inputs: boolean[]) {
    if (this.forward(inputs) > this.threshold) {
      return true;
    }

    return false;
  }

  forwardBinaryCheck(inputs: boolean[], answer: boolean) {
    if (this.forwardBinary(inputs) === answer) {
      return true;
    }

    return false;
  }

  train(inputs: boolean[], answer: boolean) {
    const result = this.forwardBinary(inputs);
    const delta = Number(answer) - Number(result);

    for (let i = 0; i < inputs.length; i++) {
      this.weights[i] += this.learningRate * delta * Number(inputs[i]);
    }

    this.threshold -= this.learningRate * delta;

    if (result === answer) {
      return true;
    }

    return false;
  }
}
