(import neuron)

(defn main [& args]
  (if (= (length args) 1)
    ((print "ERROR: training repetitions argument required")
    (os/exit 1)))

  # get number of training repetitions 
  (def reps (scan-number (args 1)))

  (def n neuron/Neuron)
  (for i 0 reps
    (var x (> (math/random) 0.5))
    (var xr (neuron/boolToReal x))
    (var y (> (math/random) 0.5))
    (var yr (neuron/boolToReal y))
  
    #(var answer (neuron/boolToReal (or x y))) # OR gate
    (var answer (neuron/boolToReal (and x y))) # AND gate
  
    (:train n @[xr yr] answer))
  
  # OR gate results
  #(print "0 || 0: " (:forwardBinary n @[0.0 0.0]))
  #(print "0 || 1: " (:forwardBinary n @[0.0 1.0]))
  #(print "1 || 0: " (:forwardBinary n @[1.0 0.0]))
  #(print "1 || 1: " (:forwardBinary n @[1.0 1.0]))
  
  # AND gate results
  (print "0 && 0: " (:forwardBinary n @[0.0 0.0]))
  (print "0 && 1: " (:forwardBinary n @[0.0 1.0]))
  (print "1 && 0: " (:forwardBinary n @[1.0 0.0]))
  (print "1 && 1: " (:forwardBinary n @[1.0 1.0]))
)
