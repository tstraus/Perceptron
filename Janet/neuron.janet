(defn boolToReal [b]
  (var result 0.0)
  (if (= b true)
    (set result 1.0))

  result)

# seed the random number generator
(math/seedrandom (os/time))

(def Neuron
  "Emulates a neuron. All inputs and outputs are real numbers between 0 and 1"
  @{:type "Neuron"
    :weights @[(math/random) (math/random)]
    :threshold (math/random)
    :learningRate 0.01
    :forward (fn [self inputs]
      (var sum 0.0)
      (for i 0 (length inputs)
        (+= sum (* (inputs i) ((self :weights) i))))
      
      (math/tanh sum))

    :forwardBinary (fn [self inputs]
      (boolToReal (> (:forward self inputs) (self :threshold))))
    
    :train (fn [self inputs answer]
      (var result (:forwardBinary self inputs))
      (var delta (- answer result))
      
      (for i 0 (length inputs)
        (+= ((self :weights) i) (* (self :learningRate) delta (boolToReal (inputs i)))))

      (-= (self :threshold) (* (self :learningRate) delta))
      
      (= result answer))})
